from xml.etree import ElementTree as ET

tree = ET.parse("movies.xml")
root = tree.getroot()


def get_description(title: str):
    for movie in root.iter("movie"):
        if movie.get("title") == title:
            return movie.find("description").text


description = get_description("Ferris Bueller's Day Off")
print(description)
